import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';

function BottomNav({state, descriptors, navigation}) {
  return (
    <View
      style={{
        backgroundColor: 'white',
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'space-between',
        shadowOpacity: 10,
        elevation: 5,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        const getIcon = label => {
          switch (label) {
            case 'Home':
              return require('./assets/Home.png');

            case 'My Bookings':
              return require('./assets/booking.png');

            case 'Help':
              return require('./assets/help.png');

            case 'Profile':
              return require('./assets/profile.png');
          }
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1, alignItems: 'center'}}>
            <Image source={getIcon(label)} />
            <Text style={{color: isFocused ? '#2E3283' : '#222'}}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

export default BottomNav;
