/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity,
  Pressable,
  Alert,
  ToastAndroid,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default function Register({navigation}) {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const showToast = () => {
    ToastAndroid.show('register success!', ToastAndroid.SHORT);
  };

  return (
    <View
      style={{
        backgroundColor: '#F7F7F7',
        flex: 1,
      }}>
      <View style={styles.content}>
        <Image source={require('./assets/logo_marlin.png')} />
        <Text style={styles.registerText}>Create an Account</Text>
        <TextInput style={styles.regInput} placeholder="Name" />
        <TextInput style={styles.regInput} placeholder="Email" />
        <TextInput style={styles.regInput} placeholder="Phone" />
        <TextInput style={styles.regInput} placeholder="Password" />
        <TouchableOpacity
          style={styles.btnRegister}
          onPress={() => showToast()}>
          <Text style={styles.btnText}>Register</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Already have account?</Text>
        <View>
          <Text
            style={{fontWeight: 'bold'}}
            onPress={() => navigation.navigate('Login')}>
            Log In
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    marginTop: 130,
    paddingHorizontal: 30,
    paddingBottom: 16,
    alignItems: 'center',
  },
  registerText: {
    marginTop: 10,
    fontSize: 20,
    marginBottom: 20,
  },
  regInput: {
    backgroundColor: 'white',
    alignSelf: 'flex-start',
    paddingHorizontal: 20,
    fontSize: 18,
    borderRadius: 10,
    marginBottom: 15,
    width: '100%',
  },
  btnRegister: {
    backgroundColor: '#2E3283',
    paddingVertical: 10,
    borderRadius: 10,
    width: '100%',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 20,
    color: 'white',
  },
  footer: {
    backgroundColor: 'white',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 40,
    paddingVertical: 5,
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowOpacity: 0.58,
    shadowRadius: 20.0,
    elevation: 50,
  },
  footerText: {
    fontSize: 16,
  },
});
