import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Homepage from './Homepage';
import ResetPassword from './ResetPassword';
import MyBookings from './MyBookings';
import ProfilePage from './ProfilePage';
import HelpPage from './HelpPage';
import BottomNav from './BottomNav';

const Tab = createBottomTabNavigator();

const BottomTab = () => {
  return (
    <Tab.Navigator
      tabBar={props => <BottomNav {...props} />}
      screenOptions={{headerShown: false}}>
      <Tab.Screen name="Home" component={Homepage} />
      <Tab.Screen name="My Bookings" component={MyBookings} />
      <Tab.Screen name="Help" component={HelpPage} />
      <Tab.Screen name="Profile" component={ProfilePage} />
    </Tab.Navigator>
  );
};

export default BottomTab;

const styles = StyleSheet.create({});
