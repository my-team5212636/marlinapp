/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity,
  Pressable,
  Alert,
  ToastAndroid,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default function ResetPassword({navigation}) {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const showToast = () => {
    ToastAndroid.show('Request sent!', ToastAndroid.SHORT);
  };

  return (
    <View
      style={{
        backgroundColor: '#F7F7F7',
        flex: 1,
        paddingHorizontal: 13,
        paddingTop: 45,
      }}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={require('./assets/backButton.png')} />
      </TouchableOpacity>

      <View style={styles.content}>
        <Image source={require('./assets/logo_marlin.png')} />
        <Text style={styles.resetTitle}>Reset Your Password</Text>
        <TextInput style={styles.inputText} placeholder="Email" />
        <TouchableOpacity style={styles.btnReset} onPress={() => showToast()}>
          <Text style={styles.textButton}>Request Reset</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    marginTop: 100,
    paddingHorizontal: 30,
    paddingBottom: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  resetTitle: {
    marginTop: 10,
    fontSize: 20,
    marginBottom: 20,
  },
  inputText: {
    backgroundColor: 'white',
    alignSelf: 'flex-start',
    paddingHorizontal: 20,
    fontSize: 18,
    borderRadius: 10,
    marginBottom: 15,
    width: '100%',
  },
  btnReset: {
    backgroundColor: '#2E3283',
    paddingVertical: 10,
    borderRadius: 10,
    width: '100%',
    alignItems: 'center',
    marginBottom: 17,
  },
  textButton: {
    fontSize: 20,
    color: 'white',
  },
});
