/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity,
  Pressable,
  Alert,
  ToastAndroid,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

// const Tab = createBottomTabNavigator();
const Homepage: () => Node = () => {
  return (
    <View style={styles.homeScreen}>
      <Image source={require('./assets/banner.png')} style={styles.banner} />
      <View style={{flexDirection: 'row'}}>
        <Image
          source={require('./assets/ic_ferry_intl.png')}
          style={styles.menuIcon}
        />
        <Image
          source={require('./assets/ic_ferry_domestic.png')}
          style={styles.menuIcon}
        />
        <Image
          source={require('./assets/ic_attraction.png')}
          style={styles.menuIcon}
        />
        <Image
          source={require('./assets/ic_pioneership.png')}
          style={styles.menuIcon}
        />
      </View>
      <TouchableOpacity style={styles.btnMore}>
        <Text style={styles.txtButton}>More ...</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  homeScreen: {
    backgroundColor: '#F7F7F7',
    flex: 1,
  },
  banner: {
    width: '100%',
    height: 160,
    marginBottom: 22,
  },
  menuIcon: {
    marginHorizontal: 16,
  },
  btnMore: {
    paddingVertical: 9,
    paddingHorizontal: 27,
    backgroundColor: '#2E3283',
    width: 110,
    alignSelf: 'center',
    borderRadius: 10,
    position: 'absolute',
    bottom: 0,
    marginBottom: 13,
  },
  txtButton: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default Homepage;
