/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity,
  Pressable,
  Alert,
  ToastAndroid,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Homepage from './Homepage';

export default function Login({navigation}) {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <View
      style={{
        backgroundColor: '#F7F7F7',
        flex: 1,
      }}>
      <View style={styles.content}>
        <Image source={require('./assets/logo_marlin.png')} />
        <Text style={styles.loginText}>Please sign in to continue</Text>
        <TextInput style={styles.loginInput} placeholder="Username" />
        <TextInput
          secureTextEntry={true}
          placeholder="Password"
          style={styles.loginInput}
        />
        <TouchableOpacity
          style={styles.btnLogin}
          onPress={() => navigation.navigate('Home')}>
          <Text style={styles.textButton}>Login</Text>
        </TouchableOpacity>
        <Text
          style={styles.forgotPass}
          onPress={() => navigation.navigate('Reset')}>
          Forgot Password
        </Text>
        <View style={styles.separator}>
          <View style={styles.horizontalLine} />
          <View>
            <Text style={{width: 100, textAlign: 'center', color: '#2E3283'}}>
              Login With
            </Text>
          </View>
          <View style={styles.horizontalLine} />
        </View>
        <View style={{flexDirection: 'row', marginBottom: 20}}>
          <Image
            style={{marginHorizontal: 20}}
            source={require('./assets/facebook.png')}
          />
          <Image
            style={{width: 50, height: 50, marginHorizontal: 20}}
            source={require('./assets/google.png')}
          />
        </View>
        <View style={{flexDirection: 'row', marginBottom: 20}}>
          <Text style={{marginRight: 30}}>App Version</Text>
          <Text style={{marginLeft: 10}}>2.8.3</Text>
        </View>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Don't have account?</Text>
        <View>
          <Text
            style={{fontWeight: 'bold'}}
            onPress={() => navigation.navigate('Register')}>
            Sign Up
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    marginTop: 100,
    paddingHorizontal: 30,
    paddingBottom: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginText: {
    marginTop: 10,
    fontSize: 20,
    marginBottom: 20,
  },
  loginInput: {
    flexDirection: 'row',
    backgroundColor: 'white',
    alignSelf: 'flex-start',
    paddingHorizontal: 20,
    fontSize: 18,
    borderRadius: 10,
    marginBottom: 15,
    width: '100%',
  },
  btnLogin: {
    backgroundColor: '#2E3283',
    paddingVertical: 10,
    borderRadius: 10,
    width: '100%',
    alignItems: 'center',
    marginBottom: 17,
  },
  textButton: {
    fontSize: 20,
    color: 'white',
  },
  forgotPass: {
    fontSize: 15,
    color: '#2E3283',
    marginBottom: 33,
  },
  separator: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 28,
  },
  horizontalLine: {
    flex: 1,
    height: 1,
    backgroundColor: 'black',
  },
  footer: {
    backgroundColor: 'white',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 40,
    paddingVertical: 5,
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowOpacity: 0.58,
    shadowRadius: 20.0,
    elevation: 50,
  },
  footerText: {
    fontSize: 16,
  },
});
